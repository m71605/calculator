﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Calculator
{
    public class FileReader
    {
        public static List<string> ReadFile(string path, out bool isSucsessful)
        {
            if (path == null)
            {
                throw new ArgumentNullException("Line is null");
            }
            var listString = new List<string>();
            isSucsessful = true;
            try
            {
                StreamReader streamReader = new StreamReader(path);
                while (!streamReader.EndOfStream)
                {
                    string s = streamReader.ReadLine();
                    if (s.Length>0)
                    {
                        listString.Add(s);
                    }
                }
                streamReader.Close();
            }
            catch (Exception e)
            {
               isSucsessful = false;
            }
            return listString;
        }
    }
}
