﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    
    public class CalculateResult
    {
        private Stack<double> _stackOfNumbers;
        private Stack<char> _stackOfOperators;
        private bool _errorOccurs;
        public CalculateResult()
        {
            _stackOfNumbers = new Stack<double>();
            _stackOfOperators = new Stack<char>();
            _errorOccurs = false;
        }
        public double Calculate(List<object> listOperands,out bool isSucsessful)
        {
            isSucsessful = true;
            for (int i = 0; i < listOperands.Count; i++)
            {
                if (listOperands[i] is double)
                {
                    bool changeSign= ChangeSingCheck(listOperands, i); 
                    if (changeSign)
                    {
                        _stackOfNumbers.Push(-Convert.ToDouble(listOperands[i])); //for immediately applying unary operations
                        _stackOfOperators.Pop();
                    }
                    else 
                    {
                        _stackOfNumbers.Push(Convert.ToDouble(listOperands[i]));
                    }
                   
                }
                else
                {
                    AddToStackOrCompute(Convert.ToChar(listOperands[i]));
                    if (_errorOccurs)
                    {
                        break;
                    }
                }
            }
            AddToStackOrCompute('+');  //to make calculation in the last time
            if (_errorOccurs)
            {
                isSucsessful = false;
                return 0;
            }
            return _stackOfNumbers.Pop();
        }
        private bool ChangeSingCheck(List<object> listOperands,int index)
        {
            bool changeSing = false;
            if (index == 1) 
            {
               if (Convert.ToChar(listOperands[0]) == '-')
                {
                    return true;
                }
            }

            if (index < 2) return false;

            if (( listOperands[index -1] is char)&&(listOperands[index - 2] is char))
            {
                if((Convert.ToChar(listOperands[index - 1])=='-')&&(Convert.ToChar(listOperands[index - 2]) == '('))
                {
                    changeSing = true;
                }
            }
            return changeSing;
        }
        private void AddToStackOrCompute(char symol)
        {     
            bool OneStepCalcutaionComplete = false;
            do
            {
                if ((_stackOfOperators.Count == 0)||(symol=='('))
                {
                    if (symol != ')')
                    {
                        _stackOfOperators.Push(symol);
                    }
                    OneStepCalcutaionComplete = true;
                }
                else if(symol == ')')
                {
                    if(_stackOfOperators.Peek()=='(')
                    {
                        _stackOfOperators.Pop();
                    }
                    ComputeStep();
                }
                else if (Priority(_stackOfOperators.Peek()) < Priority(symol))
                {
                    _stackOfOperators.Push(symol);
                    OneStepCalcutaionComplete = true;
                }
                else
                {
                    ComputeStep();
                }
                if(_errorOccurs)
                {
                    break;
                }
            } while (!OneStepCalcutaionComplete);

        }
        private void ComputeStep()
        {
            if (_stackOfOperators.Count==0)
            {
                return;
            }

            if ((_stackOfOperators.Peek()=='(')||(_stackOfOperators.Peek() == ')'))
            {
                return;
            }
            
            if (_stackOfNumbers.Count>=2)
            {
                double secondOperand = _stackOfNumbers.Pop();
                double firstOperand = _stackOfNumbers.Pop();
                double result = Act(firstOperand, secondOperand, _stackOfOperators.Pop());
                _stackOfNumbers.Push(result);
            }
           
        }
       private int Priority(char symol)
        {
            switch (symol)
            {
                case '+':
                    return 1;
                case '-':
                    return 1;
                case '*':
                    return 2;
                case '/':
                    return 2;
                case '(':
                    return 0;
            }
            return 0;
        }
        private double Act(double first, double second, char action)
        {
            double result = 0;
            switch (action)
            {
                case '+':
                    result = first + second;
                    break;

                case '-':
                    result = first - second;
                    break;

                case '*':
                    result = first*second;
                    break;
                case '/':
                    if (second != 0)
                    { 
                        result = first / second; 
                    }
                    else
                    {
                        _errorOccurs = true;
                    }
                    break;
            }
            return result;
        }

    }
}
