﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public enum LineProcessingResult
    {
        OK,
        DIVISION_BY_ZERO,
        PARSING_ERROR
    }
    public enum TypeOfParsingElement
    {
        NUMBER,
        OPERATOR        
    }
}
