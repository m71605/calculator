﻿using System;
using System.Collections.Generic;
using System.Globalization;


namespace Calculator
{
    public class ElementReader
    {
        public static List<object> ReadAllElements(string imputString)
        {
            var outputList = new List<object>();
            bool isSucsessParsed = true;
            while ((imputString.Length>0)&&(isSucsessParsed))
            {
                isSucsessParsed = ReadToken(ref imputString, out object outputElement, out TypeOfParsingElement typeOfElement);
                if (isSucsessParsed)
                {
                    if(typeOfElement == TypeOfParsingElement.NUMBER)
                    {
                        outputList.Add(Convert.ToDouble(outputElement));
                    }
                    else
                    {
                        outputList.Add(Convert.ToChar(outputElement));
                    }
                }

            }
            return outputList;
        }
        public static bool CheckLine(string imputString)
        {       
                if(imputString.Length==0)
                {
                    return false;
                }
                if((!Char.IsDigit(imputString[0]))&&(imputString[0]!='(')&&(imputString[0] != '-'))
                {
                    return false;
                }
                int bracketCounter = 0;
                for (int i = 0; i < imputString.Length; i++)
                {
                    if(imputString[i]=='(')
                    {
                        bracketCounter++;
                    }
                    else if (imputString[i] == ')')
                    {
                        bracketCounter--;
                    }
                    if (bracketCounter<0)
                    {
                        return false;
                    }
             
                    if (i > 0)
                    {
                        if ((imputString[i - 1] == '+') || (imputString[i - 1] == '-') || (imputString[i - 1] == '*') || (imputString[i - 1] == '/'))
                        {
                           if ((!Char.IsDigit(imputString[i])) && (imputString[i] != '('))
                           {
                            return false;
                           }
                        }
                    }
                    
                    if ((!Char.IsDigit(imputString[i])&&(imputString[i]!='+') && (imputString[i] != '-') && (imputString[i] != '*') && (imputString[i] != '/') && (imputString[i] != '(') && (imputString[i] != ')') && (imputString[i] != '.')))
                    {
                         return false;
                    }

                }
                if (bracketCounter != 0)
                {
                return false;
                }

                return true;
        }
        private static bool ReadToken(ref string imputString, out object outputElement, out TypeOfParsingElement type)
        {
            type = TypeOfParsingElement.OPERATOR;
            outputElement = default(object);
            bool isSucsessParse = false;

            char currentSimbol = imputString[0];
            if ((currentSimbol == ')') || (currentSimbol == '(') || (currentSimbol == '+') || (currentSimbol == '-') || (currentSimbol == '*') || (currentSimbol == '/'))
            {
                outputElement = currentSimbol;
                imputString = imputString.Substring(1);
                isSucsessParse = true;
            }
            else if(IndexOfCloserOperand(imputString)>-1)
            {
                int indexOfCloserOperand = IndexOfCloserOperand(imputString);
                string currentString = imputString.Substring(0, indexOfCloserOperand);
                isSucsessParse = Double.TryParse(currentString, NumberStyles.Any, CultureInfo.InvariantCulture, out double operand);
                outputElement = operand;
                imputString = imputString.Substring(indexOfCloserOperand);
                type = TypeOfParsingElement.NUMBER;
            }
            else
            {
                isSucsessParse = Double.TryParse(imputString, NumberStyles.Any, CultureInfo.InvariantCulture, out double operand);
                outputElement = operand;
                type = TypeOfParsingElement.NUMBER;
                imputString = "";
            }
            return isSucsessParse;
        }
        private static int IndexOfCloserOperand(string imputString)
        {
            const int ALLOWED_SIGNS_AMOUNT = 5;
            int[] arrayIndexes=new int[ALLOWED_SIGNS_AMOUNT];

            arrayIndexes[0] = imputString.IndexOf("+");
            arrayIndexes[1] = imputString.IndexOf("-");
            arrayIndexes[2] = imputString.IndexOf("*");
            arrayIndexes[3] = imputString.IndexOf("/");
            arrayIndexes[4] = imputString.IndexOf(")");

            int min = imputString.Length;
            for (int i = 0; i < ALLOWED_SIGNS_AMOUNT; i++)
            {
                if((arrayIndexes[i]> 0)&&(arrayIndexes[i] < min))
                {
                    min = arrayIndexes[i];
                }
            }
            if (min< imputString.Length)
            {
                return min;
            }
            return -1;
        }
           
    }
}
