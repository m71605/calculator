﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Calculator
{
    public static class FileWriter
    {
        public static void WriteFile(List<string> listString, string filePath)
        {
            if ((listString == null)||(filePath == null))
            {
                throw new ArgumentNullException("Argument is null");
            }
            try
            {
                StreamWriter streamWriter = new StreamWriter(filePath, false);
                for (int i = 0; i < listString.Count; i++)
                {
                    double result = OneLineProcessor.ProcessOneLine(listString[i], out LineProcessingResult processingResult);
                    switch (processingResult)
                        {
                            case LineProcessingResult.PARSING_ERROR:
                                streamWriter.WriteLine(listString[i] + " = ERROR IN EXPRESSION");
                                break;
                            case LineProcessingResult.DIVISION_BY_ZERO:
                                streamWriter.WriteLine(listString[i] + " = DEVISION BY ZERO");
                                break;
                            case LineProcessingResult.OK:
                                streamWriter.WriteLine(listString[i]+ " = " + Convert.ToString(result));
                                break;
                        }
                        
                    
                }
                streamWriter.Close();


            }
            catch (Exception e)
            {
               // Console.WriteLine(e.Message);
            }

            
        }
    }
}
