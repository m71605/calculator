﻿using System;
using System.Collections.Generic;

namespace Calculator
{
   
    class Program
    {        
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your expression or file path");
            string imputString = Console.ReadLine();
           
            List<string> listString = FileReader.ReadFile(imputString, out bool isSucsessful);

            if(isSucsessful)
            {
                int dotIndex = imputString.IndexOf('.');
                string writePath = imputString.Insert(dotIndex, "Result"); 
                FileWriter.WriteFile(listString, writePath);
            }
            else
            {
                double result = OneLineProcessor.ProcessOneLine(imputString, out LineProcessingResult processingResult);
                switch (processingResult)
                {
                    case LineProcessingResult.PARSING_ERROR:
                        Console.WriteLine("Expression is uncorrect");
                        break;
                    case LineProcessingResult.DIVISION_BY_ZERO:
                        Console.WriteLine("Division by zero is not allowed");
                        break;
                    case LineProcessingResult.OK:
                        Console.WriteLine(Convert.ToString(result));
                        break;
                }
            }


            
            
            
            
        }
    }
}
