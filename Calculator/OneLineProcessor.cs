﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public static class OneLineProcessor
    {
        public static double ProcessOneLine(string imputString, out LineProcessingResult processingResult)
        {
            imputString = imputString.Replace(" ", "");
            imputString = imputString.Replace(',', '.');
            processingResult = LineProcessingResult.OK;
            double result = 0;

            bool isCorrectLine = ElementReader.CheckLine(imputString);
            if (isCorrectLine)
            {
                var listOfOperands = ElementReader.ReadAllElements(imputString);
                var calculator = new CalculateResult();
                result = calculator.Calculate(listOfOperands, out bool isCalculationSucsessful);
                if (!isCalculationSucsessful)
                {
                    processingResult = LineProcessingResult.DIVISION_BY_ZERO;
                }
            }
            else
            {
                processingResult = LineProcessingResult.PARSING_ERROR;
            }
            return result;
        }
    }
}
