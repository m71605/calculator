﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator.Tests
{
    [TestClass()]
    public class ElementReaderTests
    {
        [DataTestMethod]
        [DataRow("5/0","5 / 0")]
        [DataRow("7*(125-19)", "7 * ( 125 - 19 )")]
        public void ReadAllElementsTest(string imputString, string expectedResult)
        {
            //arrange
            //act
            var listOfOperands = ElementReader.ReadAllElements(imputString);
            string actualResult = String.Join(" ", listOfOperands);
             // assert
            Assert.AreEqual(expectedResult, actualResult);
        }
        [DataTestMethod]
        [DataRow("5/0", true)]
        [DataRow("7*(125-19)", true)]
        [DataRow("-5", true)]
        [DataRow("((5))", true)]
        [DataRow("-5*(-6-2)", true)]
        [DataRow("-5)", false)]
        [DataRow("*5+122-14", false)]
        [DataRow("5*(12-(3)", false)]
        [DataRow("*5+122-14", false)]
        [DataRow(")5+3(", false)]
        public void CheckLineTest(string imputString, bool expectedResult)
        {
            //arrange
            //act
            bool actualResult = ElementReader.CheckLine(imputString);
            // assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}