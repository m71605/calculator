﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Calculator.Tests
{
    [TestClass()]
    public class CalculateResultTests
    {
        [DataTestMethod]
        [DataRow(new[] { "5", "*", "2" }, 10)]
        [DataRow(new[] { "2", "-", "3", "*","5"}, -13)]
        [DataRow(new[] { "(", "6", ")" }, 6)]
        [DataRow(new[] { "(", "7", "-", "4", ")","*","12" }, 36)]
        [DataRow(new[] { "(", "5", "+", "4", ")", "/", "(", "4", "-", "1", ")" }, 3)]
        [DataRow(new[] { "(", "6", ")", "*", "0" }, 0)]
        [DataRow(new[] { "(", "7.0", "-", "4.0", ")", "*", "12" }, 36)]
        [DataRow(new[] { "-", "5", }, -5)]
        public void CalculateTest_Imputline_ExpectedResult(string[] imputLines, double expectedResult)
        {
            //arrange
            List<object> listOfOperands = new List<object>();
            for (int i = 0; i < imputLines.Length; i++)
            {
                if (Double.TryParse(imputLines[i], NumberStyles.Any, CultureInfo.InvariantCulture, out double result))
                {
                    listOfOperands.Add(result);
                }
                else
                {
                    listOfOperands.Add(Convert.ToChar(imputLines[i]));
                }

            }
            var calculator = new CalculateResult();
            //act
            double actualResult = calculator.Calculate(listOfOperands, out _);
            // assert
            Assert.AreEqual(expectedResult, actualResult);
        }
        [DataTestMethod]
        [DataRow(new[] { "5", "/", "0" }, false)]
        [DataRow(new[] { "2", "-", "3", "*", "5" }, true)]
        [DataRow(new[] { "(", "6", ")", "*", "0" }, true)]
        [DataRow(new[] { "(", "7.0", "-", "4.0", ")", "*", "12" }, true)]
        [DataRow(new[] { "(", "6", ")", "/", "0" }, false)]
        [DataRow(new[] { "23", "/", "(", "7", "-", "7", ")" }, false)]
        [DataRow(new[] { "(", "2", "+", "112.9", ")", "/", "(", "5", "-", "2", "-", "3", ")" }, false)]
        public void CalculateTest_DevideByZero(string[] imputLines, bool expectedResult)
        {
            //arrange
            List<object> listOfOperands = new List<object>();
            for (int i = 0; i < imputLines.Length; i++)
            {
                if (Double.TryParse(imputLines[i], NumberStyles.Any, CultureInfo.InvariantCulture, out double result))
                {
                    listOfOperands.Add(result);
                }
                else
                {
                    listOfOperands.Add(Convert.ToChar(imputLines[i]));
                }

            }
            var calculator = new CalculateResult();
            //act
             calculator.Calculate(listOfOperands, out bool actualResult);
            // assert
            Assert.AreEqual(expectedResult, actualResult);
        }
       
    }

}