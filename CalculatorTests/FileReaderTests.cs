﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator.Tests
{
    [TestClass()]
    public class FileReaderTests
    {
        [ExpectedException(typeof(ArgumentNullException), "Exception was not thrown")]
        [TestMethod()]
        public void ReadFileTest_nullImput_Exception()
        {
            //arrange
            string path = null;

            //act
            FileReader.ReadFile(path, out _);

        }

    }
}