﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator.Tests
{
    [TestClass()]
    public class OneLineProcessorTests
    {
        
        [DataTestMethod]
        [DataRow("5*2" , 10)]
        [DataRow( "2-3*5", -13)]
        [DataRow("(6+10)/ (  3-  1)" , 8)]
        [DataRow("500*(100000000001- 100000000000)", 500)]
        [DataRow("2*(-3)", -6)]
        [DataRow("-6*(-5-1)", 36)]
        [DataRow("-18", -18)]
        [DataRow("(225)/(16-1)", 15)]
        public void ProcessOneLineTest_Imputline_ExpectedResult(string imputLine, double expectedResult)
        {
            //arrange
                        //act
            double actualResult = OneLineProcessor.ProcessOneLine(imputLine, out _);
            // assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [DataTestMethod]
        [DataRow("5*2", LineProcessingResult.OK)]
        [DataRow("2-3*5/0", LineProcessingResult.DIVISION_BY_ZERO)]
        [DataRow("(6+10)/(3-1))", LineProcessingResult.PARSING_ERROR)]
         public void ProcessOneLineTest_UncorrectLine_ExpectedResult(string imputLine, LineProcessingResult expectedResult)
        {
            //arrange
            //act
            OneLineProcessor.ProcessOneLine(imputLine, out LineProcessingResult actualResult);
            // assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }


}